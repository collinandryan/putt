all:	main

main: main.o Course.o Green.o Ball.o
	g++ -g main.o Course.o Green.o Ball.o -o putt -lSDL 

main.o: main.cpp
	g++ -c main.cpp
Course.o: Course.cpp
	g++ -c Course.cpp
Green.o: Green.cpp
	g++ -c Green.cpp
Ball.o: Ball.cpp
	g++ -c Ball.cpp

clean:
	rm -f *.o putt
