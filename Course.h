//Ryan Mackey
//April 10, 2016
//Header file for the course class. The class runs SDL graphics, and serves
//as the head of composition with greens which hold balls within it

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Green.h"
#include <vector>

#ifndef COURSE_H
#define COURSE_H

class Course{
public:
	Course(); // constructor
	void scoreBoard(); // prints scoreboard
	Green Greens[9]; // array of greens for play
	void draw(); // SDL function
	void play(); //main cyclic function
	int checkScore(); // says whather the ball is in
	void clean(); // avoid memory leaks
private:
	SDL_Surface* screen;
	int curr; // current hole
	int mx;
	int my;
};


#endif
