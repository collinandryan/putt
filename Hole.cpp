//Ryan Mackey
//April 6, 2016
//Hole Member Functions (only the constructor)

#include "Hole.h"

Hole::Hole(){
	x = 127;
	y = 144;
}

int Hole::getX(){
	return x;
}

int Hole::getY(){
	return y;
}
