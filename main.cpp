#include "Course.h"
#include <ctime>
#include <cstdlib>
#include <iostream>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

using namespace std;

int main(){
	Course course;
	course.draw();
	course.scoreBoard();
	course.play();

	course.clean();
	return 0;
}
