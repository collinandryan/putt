//Ryan Mackey
//April 10, 2016
//Hole class header file - The hole is mainly a placeholder, as it is
//static on each green.

#ifndef HOLE_H
#define HOLE_H

class Hole{
public:
	Hole();
	int getX();
	int getY();
	int x;
	int y;
};

#endif
