//Ryan Mackey
//April 10, 2016
//Header file for the Green class- the course is comprised of 9 greens, and
//each green contains its own ball, image, and specific coordinates for
//walls and the hole

#include "Ball.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>

using namespace std;

#ifndef GREEN_H
#define GREEN_H

class Green{
public:
	Green();
	~Green();
	Ball * ball;
	int getScore();
	SDL_Rect ballStart;
	SDL_Rect ballPos;
	string green;
	void moveBall(int);
	void placed();
	int place;

	//get functions
	int ballXvel();
	int ballYvel();
	int ballX();
	int ballY();

	void Wall(int);
	int score;
private:
	int curr;
	int futurexpos;
	int futureypos;
	SDL_Event event;
};
#endif
