#include "SDL/SDL.h"

int main(){

	SDL_Surface* upup = NULL;

	SDL_Surface* optimizedImage = NULL;

	SDL_Surface* screen = NULL;

	SDL_Init( SDL_INIT_EVERYTHING);

	screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE );

	upup = SDL_LoadBMP("puttputt1.bmp");

	optimizedImage = SDL_DisplayFormat( upup );
	SDL_FreeSurface( upup );

	SDL_Rect offset;
	for(int i=0; i<5; i++){
		offset.x = 0;
		offset.y = i*20;
		SDL_FillRect(screen, NULL, 0x000000);
		SDL_BlitSurface( optimizedImage, NULL, screen, &offset);
		SDL_Flip( screen);
		SDL_Delay( 1000 );
	}

	SDL_FreeSurface( optimizedImage );

	SDL_Quit();

	return 0;
}
