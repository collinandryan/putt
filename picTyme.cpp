#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

//#include "Course.h"
#include <iostream>

int main(){

	SDL_Rect offset;
	offset.x = 400;
	offset.y = 250;

	SDL_Surface* upup = NULL;
	SDL_Surface* screen = NULL;
	SDL_Surface* ball = NULL;
	SDL_Event event;

	SDL_Init( SDL_INIT_EVERYTHING);

	screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE );
	SDL_WM_SetCaption( "Golf Test", NULL);

	upup = SDL_LoadBMP("puttputt1.bmp");
	ball = SDL_LoadBMP("golf_ball.bmp");

	SDL_BlitSurface( upup, NULL, screen, NULL);

	SDL_BlitSurface( ball, NULL, screen, &offset);

	SDL_Flip( screen );

	Uint8 *keystates = SDL_GetKeyState( NULL );

//	SDL_Delay( 6000 );
	while(true){
		if( SDL_PollEvent( &event ) ){
			if( event.type == SDL_MOUSEBUTTONDOWN ){
				SDL_FreeSurface( upup );
				SDL_FreeSurface( ball );
				SDL_Quit();
				return 0;
			}
		}
		if (keystates[ SDLK_UP ] )
		{
			offset.y--;
			SDL_BlitSurface( upup, NULL, screen, NULL);
			SDL_WM_SetCaption("5!", NULL);
			SDL_BlitSurface( ball, NULL, screen, &offset);
			SDL_Flip( screen );
		}
		if (keystates[ SDLK_DOWN ] )
		{
			offset.y++;
			SDL_BlitSurface( upup, NULL, screen, NULL);

			SDL_BlitSurface( ball, NULL, screen, &offset);
			SDL_Flip( screen );
		}
		if (keystates[ SDLK_RIGHT ] )
		{
			offset.x++;
			SDL_BlitSurface( upup, NULL, screen, NULL);

			SDL_BlitSurface( ball, NULL, screen, &offset);
			SDL_Flip( screen );
		}
		if (keystates[ SDLK_LEFT ] )
		{
			offset.x--;
			SDL_BlitSurface( upup, NULL, screen, NULL);

			SDL_BlitSurface( ball, NULL, screen, &offset);
			SDL_Flip( screen );
		}
		if(offset.x > 120 && offset.x < 132 && offset.y < 150 && offset.y > 138)
			break;
	}
	SDL_Delay(250);
	SDL_FreeSurface( upup );
	SDL_FreeSurface( ball );

	SDL_Quit();
}
