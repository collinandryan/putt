//Ryan Mackey
//April 10, 2016
//Ball class - contains ball information. New ball object for every hole.

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#ifndef BALL_H
#define BALL_H

class Ball{
public:
	Ball();
	~Ball();
	int getX();
	int getY();
	SDL_Rect* getOffset();
	int xpos;
	int ypos;
	int xvel;
	int yvel;
	SDL_Rect offset;
};

#endif
