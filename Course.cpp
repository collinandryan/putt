#include "Course.h"
#include "Green.h"
#include <iostream>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <vector>
#include <string>

using namespace std;

Course::Course(){
	//Preparing SDL
	screen = SDL_SetVideoMode( 640, 400, 32, SDL_SWSURFACE );
	SDL_Init( SDL_INIT_EVERYTHING);

	//populates ana rray of 9 greens (Holes to play)
	for(int i=0; i<9; ++i){
		Greens[i] = Green();
	}

	//mouse position variables
	mx = 0;
	my = 0;

	//tracks current hole
	curr = 0;
}

//Score information between holes
void Course::scoreBoard(){
	SDL_Event event;
	int total = 0, cong = 1;
	int pars[] = {4,2,2,3,3,3,2,3,3};

	//maximum possible score
	(Greens[curr].score>9)?(Greens[curr].score=9) : (Greens[curr].score = Greens[curr].score);

	//calculates the players total score
	for(int i =0; i<9; i++){
		total += Greens[i].score;
	}

	cout << endl << endl << endl << endl << endl << endl;

	//Prints special score-based messages
	if(Greens[curr].score == 1 && cong){
		cout << "HOLE IN ONE!!!" << endl << endl;
		cong--;
	}
	if(pars[curr] - Greens[curr].score == 2 && cong){
		cout << "EAGLE!!" << endl << endl;
		cong--;
	}
	if(pars[curr] - Greens[curr].score == 1 && cong){
		cout << "BIRDIE!" << endl << endl;
		cong--;
	}
	if(pars[curr] - Greens[curr].score == 0 && cong){
		cout << "PAR" << endl << endl;
		cong--;
	}

	//prints the actual beautiful scoreboard
	cout << "+++++++++++++++++++++++++++++++++++++++++"<<endl<< "HOLE| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |"<<endl;
	cout << "-----------------------------------------"<<endl<<"PAR | 4 | 2 | 2 | 3 | 3 | 3 | 2 | 3 | 3 |"<<endl<<"-----------------------------------------"<<endl<<"YOU | "<<Greens[0].score<<" | "<<Greens[1].score<<" | "<<Greens[2].score<<" | "<<Greens[3].score<<" | "<<Greens[4].score<<" | "<<Greens[5].score<<" | "<<Greens[6].score<<" | "<<Greens[7].score<<" | "<<Greens[8].score<<" |"<<endl<<"+++++++++++++++++++++++++++++++++++++++++"<<endl;
	cout <<"TOTAL = " << total << "           TOTAL PAR = 25" << endl << "+++++++++++++++++++++++++++++++++++++++++" << endl;
	cout << endl;
	//waits for the user to click to proceed
	while(true){
		if(SDL_PollEvent(&event)){
			if( event.type == SDL_MOUSEBUTTONDOWN){
				break;
			}
		}
	}
	return;
}

//main play function for the game
void Course::play(){
	SDL_Event event;
	//plays all 9 holes
	while(curr < 9){
		//updates drawing
		draw();
		//while the ball isn't in the hole
		while(!checkScore()){
			//ball follows mouse until placement
			if(!Greens[curr].place){
				Greens[curr].ball->xvel = 0;
				Greens[curr].ball->yvel = 0;
				SDL_GetMouseState(&mx, &my);
				switch(curr){ //this is for placing it in the dark green
				case(0):
					if(mx < 497){
						Greens[curr].ballPos.x = 497;
					} else if(mx > 523) {
						Greens[curr].ballPos.x = 523;
					} else {	
						Greens[curr].ballPos.x = mx;
					}
					if(my < 254){
						Greens[curr].ballPos.y = 254;
					} else if(my > 303){
						Greens[curr].ballPos.y = 303;
					} else {
						Greens[curr].ballPos.y = my;
					}
					break;
				case(1):
					if(mx < 297){
						Greens[curr].ballPos.x = 297;
					} else if(mx > 343){
						Greens[curr].ballPos.x = 343;
					} else {
						Greens[curr].ballPos.x = mx;
					}
					if(my < 100){
						Greens[curr].ballPos.y = 100;
					} else if(my > 128){
						Greens[curr].ballPos.y = 128;
					} else {
						Greens[curr].ballPos.y = my;
					}
					break;
				case(2):
					if(mx < 164){
						Greens[curr].ballPos.x = 164;
					} else if(mx > 250){
						Greens[curr].ballPos.x = 250;
					} else {
						Greens[curr].ballPos.x = mx;
					}
					if(my < 273)
						Greens[curr].ballPos.y = 273;
					else if(my > 298)
						Greens[curr].ballPos.y = 298;
					else
						Greens[curr].ballPos.y = my;
					break;
				case(3):
					if(mx < 444) Greens[curr].ballPos.x = 444;
					else if (mx > 468) Greens[curr].ballPos.x = 468;
					else Greens[curr].ballPos.x = mx;
					if(my < 255) Greens[curr].ballPos.y = 255;
					else if(my > 297) Greens[curr].ballPos.y = 297;
					else Greens[curr].ballPos.y = my;
					break;
				case(4):
					if(mx < 205) Greens[curr].ballPos.x = 205;
					else if (mx > 229) Greens[curr].ballPos.x = 229;
					else Greens[curr].ballPos.x = mx;
					if(my < 244) Greens[curr].ballPos.y = 244;
					else if (my > 289) Greens[curr].ballPos.y = 289;
					else Greens[curr].ballPos.y = my;
					break;
				case(5):
					if(mx < 336) Greens[curr].ballPos.x = 336;
					else if(mx > 382) Greens[curr].ballPos.x = 382;
					else Greens[curr].ballPos.x = mx;
					if(my < 309) Greens[curr].ballPos.y = 309;
					else if(my > 333) Greens[curr].ballPos.y = 333;
					else Greens[curr].ballPos.y = my;
					break;
				case(6):
					if(mx < 222) Greens[curr].ballPos.x = 222;
					else if(mx > 263) Greens[curr].ballPos.x = 263;
					else Greens[curr].ballPos.x = mx;
					if(my < 211) Greens[curr].ballPos.y = 211;
					else if(my > 235) Greens[curr].ballPos.y = 235;
					else Greens[curr].ballPos.y = my;
					break;
				case(7):
					if(mx < 244) Greens[curr].ballPos.x = 244;
					else if(mx > 290) Greens[curr].ballPos.x = 290;
					else Greens[curr].ballPos.x = mx;
					if(my < 83) Greens[curr].ballPos.y = 83;
					else if (my > 111) Greens[curr].ballPos.y = 111;
					else Greens[curr].ballPos.y = my;
					break;
				case(8):
					if(mx < 203) Greens[curr].ballPos.x = 203;
					else if(mx > 228) Greens[curr].ballPos.x = 228;
					else Greens[curr].ballPos.x = mx;
					if(my < 83) Greens[curr].ballPos.y = 83;
					else if(my > 148) Greens[curr].ballPos.y = 148;
					else Greens[curr].ballPos.y = my;
					break;
				}
			}
			//upon hitting the ball, moves it appropriately
			if(Greens[curr].ballXvel() != 0 || Greens[curr].ballYvel() != 0){
				Greens[curr].moveBall(curr);
				draw();
			}
			//checks for interactions
			if( SDL_PollEvent(&event) ) {
				if( event.type == SDL_MOUSEMOTION){
					draw();
				}
				//first click places the ball
				if( event.type == SDL_MOUSEBUTTONDOWN){
					if(!Greens[curr].place){
						Greens[curr].place++;
					}
					//second click applies a stroke and velocities
					else{
						Greens[curr].score++;
						SDL_GetMouseState(&mx, &my);
						Greens[curr].ball->xvel = (Greens[curr].ballPos.x - mx)/4;
						Greens[curr].ball->yvel = (Greens[curr].ballPos.y - my)/4;
					}
				}
			}
		}
		draw();
		//after scoring, update user
		scoreBoard();
		//increment to next hole
		curr++;
	}
	return;
}

//check if the ball is in the hole
int Course::checkScore(){
	int holeX[] = {127,326,462,296,220,316,391,387,222};
	int holeY[] = {144,304,155,116,170,100,96,287,270};
	if (curr < 9 && Greens[curr].place){
		if(Greens[curr].ballPos.x < (holeX[curr] + 3) && Greens[curr].ballPos.x > (holeX[curr]-9) && Greens[curr].ballPos.y > (holeY[curr]-9) && Greens[curr].ballPos.y < (holeY[curr]+3)){
			Greens[curr].ballPos.x = holeX[curr]-3;
			Greens[curr].ballPos.y = holeY[curr]-3;
			draw();
			return 1;
		}
	}
	return 0;
}

//draw the ball at its position
void Course::draw(){

	//cycles through green picture based on current hole
	char picInfo[11]= "greenX.bmp";
	int numInfo;
	numInfo = curr+1;
	picInfo[5] = numInfo + '0';

	SDL_BlitSurface(SDL_LoadBMP(picInfo), NULL, screen, NULL);

	//creates ball with transparency
	SDL_Surface* ballbmp = SDL_LoadBMP("ball.bmp");
	ballbmp->format->Amask = 0xFF000000;
	ballbmp->format->Ashift = 24;
	SDL_SetAlpha(ballbmp, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);

	//draws a guide dot for the putter for a placed ball
	if(Greens[curr].ball->xvel == 0 && Greens[curr].ball->yvel == 0 && Greens[curr].place){
		SDL_Rect guide;
		SDL_GetMouseState(&mx, &my);
		guide.x = Greens[curr].ballPos.x-(mx-Greens[curr].ballPos.x);
		guide.y = Greens[curr].ballPos.y-(my-Greens[curr].ballPos.y);
		guide.w = 5;
		guide.h = 5;
		SDL_BlitSurface(SDL_LoadBMP("guide.bmp"), NULL, screen, &guide);
	}

	SDL_BlitSurface(ballbmp, NULL, screen, &Greens[curr].ballPos);

	SDL_Flip( screen );

}

void Course::clean(){
	SDL_FreeSurface(screen);
}
