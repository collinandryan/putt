//Ryan Mackey Collin Klenke
//April 10, 2016
//Member functions and constructor for the Green class
#include "Green.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <iostream>
#include <string>
#include "Course.h"
#include <cmath>


using namespace std;

Green::Green(){
	ball = new Ball;

	//Information for an SDL_Rect
	ballStart.x = ball->xpos;
	ballStart.y = ball->ypos;
	ballStart.w = 10;
	ballStart.h = 10;

	//is ball placed?
	place = 0;
	ballPos = ballStart;
	score = 0;
	curr = 0;
}

Green::~Green(){
}

int Green::ballXvel(){
	return ball->xvel;
}

int Green::ballYvel(){
	return ball->yvel;
}

int Green::ballX(){
	return ball->xpos;
}

int Green::ballY(){
	return ball->ypos;
}

void Green::placed(){
//	cout << "in placed" << endl;
//	ball.getXV();
//	while(ball.xvel != 0 || ball.vel != 0){
//		ball.move();
//	}
//	cout << "out of while" << endl;
	return;
}

int Green::getScore(){
	return score;
}

//moves the ball - called from Course
void Green::moveBall(int current){
	curr = current;

	//determines x and y directions of the ball
	int yneg, xneg;
	(ball->xvel<1)?(xneg=-1) : (xneg=1);
	(ball->yvel<1)?(yneg=-1) : (yneg=1);

	//decreases velocity by an appropriate fraction
	ball->xvel = (ball->xvel*14/15);
	ball->yvel = (ball->yvel*14/15);

	//makes the movement of the ball more natural when x>>y or x<<y
	if(ball->xvel == 0 && ball->yvel != 0)
		ball->xvel = 1*xneg;
	if(ball->yvel == 0 && ball->xvel != 0)
		ball->yvel = 1*yneg;

	//prepare for collision detection
	futurexpos = ballPos.x + ball->xvel;
	futureypos = ballPos.y + ball->yvel;

	Wall(curr);

	SDL_Delay(65);
}

//checks for collisions with walls- changes velocity and position as needed
void Green::Wall(int curr){
//each case is coded for the [curr] green
	switch(curr){
	case 0:
		if(ballPos.y >= 255 && ballPos.y <= 311 && ballPos.x <= 530 && ballPos.x >= 169){
			if(futureypos >= 304){
				ball->yvel = -1 * ball->yvel; //I expect we will use some OOP to make Collision be able to change yvel.
				futureypos = 304;
			}
			if(futureypos <= 255){
				ball->yvel = -1 * ball->yvel;
				futureypos = 255;
			}
			if (futurexpos >= 523){
				ball->xvel = -1 * ball->xvel;
				futurexpos = 523;
			}
		}
		if(ballPos.y >= 184 && ballPos.y < 311 && ballPos.x >= 99 && ballPos.x < 169){
			if(futurexpos >= 162 && ballPos.y <= 255 && ballPos.y >= 237){
				ball->xvel = -1*ball->xvel;
				futurexpos = 162;
			}
			if(futurexpos <= 99){
				ball->xvel = -1*ball->xvel;
				futurexpos = 99;
			}
			if(futureypos >= 304){
				ball->yvel = -1*ball->yvel;
				futureypos = 304;
			}
			if(futureypos <= 184){
				ball->yvel = -1*ball->yvel;
				futureypos = 184;
			}
		}
		if(ballPos.y >= 184 && ballPos.y <= 244 && ballPos.x >= 168 && ballPos.x <= 462){
			if(futureypos <= 184){
				ball->yvel = -1*ball->yvel;
				futureypos = 184;
			}
			if(futureypos >= 237){
				ball->yvel = -1*ball->yvel;
				futureypos = 237;
			}
		}
		if(ballPos.y >= 120 && ballPos.y <= 244 && ballPos.x >= 462 && ballPos.x <= 530){
			if(futurexpos <= 462 && ballPos.y >= 166 && ballPos.y <= 184){
				ball->xvel = -1*ball->xvel;
				futurexpos = 462;
			}
			if(futureypos <= 120){
				ball->yvel = -1*ball->yvel;
				futureypos = 120;
			}
			if(futureypos >= 237){
				ball->yvel = -1*ball->yvel;
				futureypos = 237;
			}
			if(futurexpos >= 523){
				ball->xvel = -1*ball->xvel;
				futurexpos = 523;
			}
		}
		if(ballPos.y >= 120 && ballPos.y <= 173 && ballPos.x >= 99 && ballPos.x <= 462){
			if(futurexpos <= 99){
				ball->xvel = -1*ball->xvel;
				futurexpos = 99;
			}
			if(futureypos <= 120){
				ball->yvel = -1*ball->yvel;
				futureypos = 120;
			}
			if(futureypos >= 166){
				ball->yvel = -1*ball->yvel;
				futureypos = 166;
			}
		}
		if(futurexpos >= 523)
			futurexpos = 523;
		if(futurexpos <= 99)
			futurexpos = 99;
		if(futureypos >= 304)
			futureypos = 304;
		if(futureypos <= 118)
			futureypos = 118;
		break;
	case(1): //could use some work
		if(ballPos.y < 217 && ballPos.x > 325 && futureypos > 217){ //works
			ball->yvel = -1*ball->yvel;
			futureypos = 217;
		}
		if(ballPos.y > 217 && ballPos.y < 235 && ballPos.x < 323 && futurexpos > 323){
			ball->xvel = -1*ball->xvel;
			futurexpos = 323;
		}
		if(ballPos.y > 232 && ballPos.x > 325 && futureypos < 232){
			ball->yvel = -1*ball->yvel;
			futureypos = 232;
		}
		if(ballPos.y < 241 && ballPos.x < 315 && futureypos > 241){
			ball->yvel = -1*ball->yvel;
			futureypos = 241;
		}
		if(ballPos.y > 238 && ballPos.y < 257 && ballPos.x > 318 && futurexpos < 318){
			ball->xvel = -1*ball->xvel;
			futurexpos = 318;
		}
		if(ballPos.y > 257 && ballPos.x < 318 && futureypos < 257){
			ball->yvel = -1*ball->yvel;
			futureypos = 257;
		}
		//corner things
		if(ballPos.y < 217 && ballPos.x < 325 && futureypos > 217 && futurexpos > 325){
			ball->xvel = -1*ball->xvel;
			futurexpos = 325;
			futureypos = 227;
		}
		if(futureypos < 96){
			ball->yvel = -1*ball->yvel;
			futureypos = 96;
		}
		if(futureypos > 330){
			ball->yvel = -1*ball->yvel;
			futureypos = 330;
		}
		if(futurexpos < 283){
			ball->xvel = -1*ball->xvel;
			futurexpos = 283;
		}
		if(futurexpos > 362){
			ball->xvel = -1*ball->xvel;
			futurexpos = 362;
		}
		break;
	case(2):
		if(ballPos.x < 266 && ballPos.x > 153 && ballPos.y < 299 && ballPos.y > 203){
			if(futurexpos > 266){
				ball->xvel = -1*ball->xvel;
				futurexpos = 266;
			}
			if(futurexpos < 153){
				ball->xvel = -1*ball->xvel;
				futurexpos = 153;
			}
			if(futureypos > 299){
				ball->yvel = -1*ball->yvel;
				futureypos = 299;
			}
			if(ballPos.x > 172 && ballPos.x < 230 && futureypos < 218){
				futureypos = 218;
				ball->yvel = -1*ball->yvel;
			}
			if(ballPos.y < 218 && ballPos.x > 238 && futurexpos < 238){
				ball->xvel = -1*ball->xvel;
				futurexpos = 238;
			}
			if(ballPos.y < 218 && ballPos.x < 166 && futurexpos > 166){
				ball->xvel = -1*ball->xvel;
				futurexpos = 166;
			}
		}
		if(ballPos.x >= 153 && ballPos.x <= 472 && ballPos.y > 102 && ballPos.y < 203){
			if(futurexpos <= 153){
				ball->xvel = -1*ball->xvel;
				futurexpos = 153;
			}
			if(futurexpos >= 472){
				ball->xvel = -1*ball->xvel;
				futurexpos = 472;
			}
			if(ballPos.x > 272 && futureypos >= 203){
				ball->yvel = -1*ball->yvel;
				futureypos = 203;
			}
			if(futureypos <= 102){
				ball->yvel = -1*ball->yvel;
				futureypos = 102;
			}
			if(ballPos.x > 171 && ballPos.x < 230 && futureypos >= 203){
				ball->yvel = -1*ball->yvel;
				futureypos = 203;
			}
		}
		if(futurexpos < 155){
			//ball->xvel = -1*ball->xvel;
			futurexpos = 155;
		}
		if(futurexpos > 472){
			ball->xvel = -1*ball->xvel;
			futurexpos = 472;
		}
		break;
	case(3):
		if(ballPos.x <= 474 && ballPos.x >= 160 && ballPos.y >= 234 && ballPos.y <= 316){
			if(futurexpos <= 160){
				futurexpos = 160;
				ball->xvel = -1*ball->xvel;
			}
			if(futurexpos >= 474){
				ball->xvel = -1*ball->xvel;
				futurexpos = 474;
			}
			if(futureypos >= 316){
				futureypos = 316;
				ball->yvel = -1*ball->yvel;
			}
			if(ballPos.x <= 250 && futureypos < 234){
				futureypos = 234;
				ball->yvel = -1*ball->yvel;
			}
			if(ballPos.x >= 307 && futureypos < 234){
				futureypos = 234;
				ball->yvel = -1*ball->yvel;
			}
		}
		if(ballPos.x >= 250 && ballPos.x <= 307 && ballPos.y <= 234 && ballPos.y >= 169){
			if(futurexpos <= 250){
				ball->xvel = -1*ball->xvel;
				futurexpos = 250;
			}
			if(futurexpos >= 307){
				ball->xvel = -1*ball->xvel;
				futurexpos = 307;
			}
		}
		if(ballPos.x >= 223 && ballPos.x <= 360 && ballPos.y <= 170 && ballPos.y >= 75){
			if(futurexpos <= 223){
				ball->xvel = -1*ball->xvel;
				futurexpos = 223;
			}
			if(futurexpos >= 360){
				ball->xvel = -1*ball->xvel;
				futurexpos = 360;
			}
			if(ballPos.x <= 250 && futureypos >= 170){
				ball->yvel = -1*ball->yvel;
				futureypos = 170;
			}
			if(ballPos.x >= 307 && futureypos >= 170){
				ball->yvel = -1*ball->yvel;
				futureypos = 170;
			}
			if(futureypos <= 75){
				ball->yvel = -1*ball->yvel;
				futureypos = 75;
			}
		}
		break;
	case(4):
		if(futurexpos <= 205){
			ball->xvel = -1*ball->xvel;
			futurexpos = 205;
		}
		if(futurexpos >= 436){
			ball->xvel = -1*ball->xvel;
			futurexpos = 436;
		}
		if(futureypos <= 148){
			ball->yvel = -1*ball->yvel;
			futureypos = 148;
		}
		if(futureypos >= 289){
			ball->yvel = -1*ball->yvel;
			futureypos = 289;
		}
		if(futureypos <= 244 && futureypos >= 227 && futurexpos <= 284 && futurexpos >= 205){
			if(ballPos.x <= 284){
				ball->yvel = -1*ball->yvel;
				if(ballPos.y < 227){
					futureypos = 227;
				} else {
					futureypos = 244;
				}
			} else {
				ball->xvel = -1*ball->xvel;
				futurexpos = 284;
			}
		}
		if(futureypos <= 244 && futureypos >= 227 && futurexpos <= 436 && futurexpos >= 354){
			if(ballPos.x >= 354){
				ball->yvel = -1*ball->yvel;
				if(ballPos.y < 227){
					futureypos = 227;
				} else {
					futureypos = 244;
				}
			} else {
				ball->xvel = -1*ball->xvel;
				futurexpos = 354;
			}
		}
		if(futureypos <= 210 && futureypos >= 192 && futurexpos <= 284 && futurexpos >= 205){
			if(ballPos.x <= 284){
				ball->yvel = -1*ball->yvel;
				if(ballPos.y < 192){
					futureypos = 192;
				} else {
					futureypos = 210;
				}
			} else {
				ball->xvel = -1*ball->xvel;
				futurexpos = 284;
			}
		}
		if(futureypos <= 210 && futureypos >= 192 && futurexpos <= 436 && futurexpos >= 354){
			if(ballPos.x <= 354){
				ball->yvel = -1*ball->yvel;
				if(ballPos.y < 192){
					futureypos = 192;
				} else {
					futureypos = 210;
				}
			} else {
				ball->xvel = -1*ball->xvel;
				futurexpos = 354;
			}
		}
		break;
	case(5):
		if(ballPos.y >= 267 && ballPos.y <= 338){
			if(futurexpos <= 300){
				ball->xvel = -1*ball->xvel;
				futurexpos = 300;
			}
			if(futurexpos >= 417){
				ball->xvel = -1*ball->xvel;
				futurexpos = 417;
			}
			if(futureypos >= 338){
				ball->yvel = -1*ball->yvel;
				futureypos = 338;
			}
			if(ballPos.x <= 348 && futureypos <= 267){
				ball->yvel = -1*ball->yvel;
				futureypos = 267;
			}
			if(ballPos.x >= 370 && futureypos <= 267){
				ball->yvel = -1*ball->yvel;
				futureypos = 267;
			}
		}
		if(ballPos.y < 267 && ballPos.y > 112){
			if(futurexpos <= 348){
				ball->xvel = -1*ball->xvel;
				futurexpos = 348;
			}
			if(futurexpos >= 370){
				ball->xvel = -1*ball->xvel;
				futurexpos = 370;
			}
		}
		if(ballPos.y <= 112 && ballPos.y >= 43){
			if(futurexpos <= 300){
				ball->xvel = -1*ball->xvel;
				futurexpos = 300;
			}
			if(futurexpos >= 417){
				ball->xvel = -1*ball->xvel;
				futurexpos = 417;
			}
			if(futureypos <= 43){
				ball->yvel = -1*ball->yvel;
				futureypos = 43;
			}
			if(ballPos.x <=348 && futureypos >= 112){
				ball->yvel = -1*ball->yvel;
				futureypos = 112;
			}
			if(ballPos.x >= 370 && futureypos >= 112){
				ball->yvel = -1*ball->yvel;
				futureypos = 112;
			}
		}
		break;
	case(6):
		if(ballPos.x <= 276 && ballPos.x >= 211){
			if(futureypos <= 205){
				ball->yvel = -1*ball->yvel;
				futureypos = 205;
			}
			if(futureypos >= 318){
				ball->yvel = -1*ball->yvel;
				futureypos = 318;
			}
			if(futurexpos <= 211){
				ball->xvel = -1*ball->xvel;
				futurexpos = 211;
			}
			if(ballPos.y <= 264 && futurexpos >= 276){
				ball->xvel = -1*ball->xvel;
				futurexpos = 276;
			}
		}
		if(ballPos.x  > 276 && ballPos.x < 354){
			if(futureypos >= 318){
				ball->yvel = -1*ball->yvel;
				futureypos = 318;
			}
			if(futureypos <= 264){
				ball->yvel = -1*ball->yvel;
				futureypos = 264;
			}
		}
		if(ballPos.x > 354 && ballPos.x <= 421){
			if(futureypos >= 318){
				ball->yvel = -1*ball->yvel;
				futureypos = 318;
			}
			if(futureypos <= 69){
				ball->yvel = -1*ball->yvel;
				futureypos = 69;
			}
			if(futurexpos >= 421){
				ball->xvel = -1*ball->xvel;
				futurexpos = 421;
			}
			if(ballPos.y <= 264 && futurexpos <= 354){
				ball->xvel = -1*ball->xvel;
				futurexpos = 354;
			}
		}
		break;
	case(7):
		if(ballPos.x >= 228 && ballPos.x <= 308 && ballPos.y >= 79 && ballPos.y <= 313){
			if(futureypos <= 79){
				ball->yvel = -1*ball->yvel;
				futureypos = 79;
			}
			if(futureypos >= 313){
				ball->yvel = -1*ball->yvel;
				futureypos = 313;
			}
			if(futurexpos <= 228){
				ball->xvel = -1*ball->xvel;
				futurexpos = 228;
			}
			if(ballPos.y <= 253 && futurexpos >= 308){
				ball->xvel = -1*ball->xvel;
				futurexpos = 308;
			}
			if(futureypos >= 160 && futureypos <= 175 && futurexpos <= 264 && futurexpos >= 228){
				if(ballPos.x <= 264){
					ball->yvel = -1*ball->yvel;
					if(ballPos.y <= 160){
						futureypos = 160;
					} else {
						futureypos = 175;
					}
				} else {
					ball->xvel = -1*ball->xvel;
					futurexpos = 264;
				}
			}
			if(futureypos <= 231 && futureypos >= 216 && futurexpos <= 315 && futurexpos >= 270){
				if(ballPos.x >= 270){
					ball->yvel = -1*ball->yvel;
					if(ballPos.y <= 216){
						futureypos = 216;
					} else {
						futureypos = 231;
					}
				} else {
					ball->xvel = -1*ball->xvel;
					futurexpos = 270;
				}
			}	
		}
		if(ballPos.y <= 313 && ballPos.y >= 253 && ballPos.x <= 399 && ballPos.x > 308){
			if(futureypos >= 313){
				ball->yvel = -1*ball->yvel;
				futureypos = 313;
			}
			if(futureypos <= 253){
				ball->yvel = -1*ball->yvel;
				futureypos = 253;
			}
			if(futurexpos >= 399){
				ball->xvel = -1*ball->xvel;
				futurexpos = 399;
			}
		}
		break;
	case(8):
		if(ballPos.x >= 203 && ballPos.x <= 353 && ballPos.y >= 83 && ballPos.y <= 148){
			if(futurexpos <= 203){
				ball->xvel = -1*ball->xvel;
				futurexpos = 203;
			}
			if(futureypos <= 83){
				ball->yvel = -1*ball->yvel;
				futureypos = 83;
			}
			if(ballPos.x <= 342 && futureypos >= 148){
				ball->yvel = -1*ball->yvel;
				futureypos = 148;
			}
			if(ballPos.y <= 101 && futurexpos >= 353){
				ball->xvel = -1*ball->xvel;
				futurexpos = 353;
			}
			if(futurexpos >= 353 && futurexpos <= 370 && futureypos >= 148){
				futurexpos = 360;
				futureypos = 148;
				ball->yvel = -1*ball->yvel;
			}
		}
		if(ballPos.x > 353 && ballPos.x <= 430 && ballPos.y >= 101 && ballPos.y <= 281){
			if(futureypos <= 101){
				ball->yvel = -1*ball->yvel;
				futureypos = 101;
			}
			if(futureypos >= 281){
				ball->yvel = -1*ball->yvel;
				futureypos = 281;
			}
			if(futurexpos >= 430){
				ball->xvel = -1*ball->xvel;
				futurexpos = 430;
			}
			/*if(ballPos.y <= 235 && ballPos.y >= 148 && futurexpos <= 370){
				ball->xvel = -1*ball->xvel;
				futurexpos = 370;
			}*/
			if(futurexpos <= 370 && futureypos <= 235 && futureypos >= 148){
				if(ballPos.y <= 235 && ballPos.y >= 148){
					ball->xvel = -1*ball->xvel;
					futurexpos = 370;
				} else {
					ball->yvel = -1*ball->yvel;
					if(ballPos.y > 235){
						futureypos = 235;
					} else {
						futureypos = 148;
					}
				}
			}
		}
		if(ballPos.y > 148 && ballPos.y <= 234 && ballPos.x <= 353 && ballPos.x >= 342){//middle section thing
			if(futurexpos >= 353){
				ball->xvel = -1*ball->xvel;
				futurexpos = 353;
			}
			if(futurexpos <= 342){
				ball->xvel = -1*ball->xvel;
				futurexpos = 342;
			}
		}
		if(ballPos.x >= 203 && ballPos.x <= 353 && ballPos.y >= 234 && ballPos.y <= 300){
			if(futureypos >= 300){
				ball->yvel = -1*ball->yvel;
				futureypos = 300;
			}
			if(ballPos.x <= 342 && futureypos <= 234){
				ball->yvel = -1*ball->yvel;
				futureypos = 234;
			}
			if(futurexpos <= 203){
				ball->xvel = -1*ball->xvel;
				futurexpos = 203;
			}
			if(ballPos.y >= 281 && futurexpos >= 353){
				ball->xvel = -1*ball->xvel;
				futurexpos = 353;
			}
		}
		break;				
				
	}
	//finally moves the ball as is appropriate
	ballPos.x  = futurexpos;
	ballPos.y = futureypos;
}
