//Ryan Mackey
//April 10, 2016
//Ball class- mainly stores data on the ball, and allows for easy resets in each new hole. SDL information.

#include "Ball.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <iostream>

using namespace std;

Ball::Ball(){
	xpos = 400;
	ypos = 200;
	xvel = 0;
	yvel = 0;
	offset.x = xpos;
	offset.y = ypos;

}

Ball::~Ball(){
}

int Ball::getX(){
	return xpos;
}

int Ball::getY(){
	return ypos;
}

//SDL information for game engine
SDL_Rect* Ball::getOffset(){
	return &offset;
}
